import React from 'react';
import logo from './logo.svg';
import './App.css';
import One from '@catman/one'

function App() {
  console.log('@catman/one', One)
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
